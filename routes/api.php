<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CitizenshipController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\ContractController;
use App\Http\Controllers\DomesticRoleController;
use App\Http\Controllers\EducationController;
use App\Http\Controllers\EmployerController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\ResumeController;
use App\Http\Controllers\VacancyController;
use App\Http\Controllers\WorkerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group([
    'middleware' => 'auth:api-employers,api-workers',
    'prefix' => 'contracts'
], function () {
    Route::post('/', [ContractController::class, 'store']);
    Route::delete('/{contract}', [ContractController::class, 'destroy']);
    Route::patch('/{contract}', [ContractController::class, 'update']);
});

Route::group(['prefix' => 'vacancies'], function () {
    Route::get('/', [VacancyController::class, 'index']);
    Route::post('/', [VacancyController::class, 'store'])->middleware('auth:api-employers');
    Route::get('/{vacancy}', [VacancyController::class, 'show']);
    Route::patch('/{vacancy}', [VacancyController::class, 'update'])->middleware('auth:api-employers');
    Route::delete('/{vacancy}', [VacancyController::class, 'destroy'])->middleware('auth:api-employers');
});

Route::group(['prefix' => 'resumes'], function () {
    Route::get('/', [ResumeController::class, 'index']);
    Route::post('/', [ResumeController::class, 'store'])->middleware('auth:api-workers');
    Route::get('/{resume}', [ResumeController::class, 'show']);
    Route::patch('/{resume}', [ResumeController::class, 'update'])->middleware('auth:api-workers');
    Route::delete('/{resume}', [ResumeController::class, 'destroy'])->middleware('auth:api-workers');
});

Route::group(['prefix' => 'employers/{employer}'], function () {
    Route::get('/', [EmployerController::class, 'showEmployerData']);
    Route::get('/contracts', [EmployerController::class, 'showEmployerContracts']);
    Route::get('/vacancies', [EmployerController::class, 'showEmployerVacancies']);
    Route::patch('/', [EmployerController::class, 'update'])->middleware('auth:api-employers');
});

Route::group(['prefix' => 'workers/{worker}'], function () {
    Route::get('/', [WorkerController::class, 'showWorkerData']);
    Route::get('/contracts', [WorkerController::class, 'showWorkerContracts']);
    Route::get('/resumes', [WorkerController::class, 'showWorkerResumes']);
    Route::patch('/', [WorkerController::class, 'update'])->middleware('auth:api-workers');
});

Route::get('cities', [CityController::class, 'index']);
Route::get('educations', [EducationController::class, 'index']);
Route::get('languages', [LanguageController::class, 'index']);
Route::get('citizenships', [CitizenshipController::class, 'index']);
Route::get('roles', [DomesticRoleController::class, 'index']);

Route::group(['prefix' => 'auth'], function () {
    Route::group(
        ['prefix' => 'employers'],
        function () {
            Route::post('registration', [EmployerController::class, 'registration']);
            Route::post('login', [EmployerController::class, 'login']);
            Route::get('whoiam', [EmployerController::class, 'whoiam'])->middleware('auth:api-employers');
        }
    );

    Route::group(
        ['prefix' => 'workers'],
        function () {
            Route::post('registration', [WorkerController::class, 'registration']);
            Route::post('login', [WorkerController::class, 'login']);
            Route::get('whoiam', [WorkerController::class, 'whoiam'])->middleware('auth:api-workers');
        }
    );

    Route::post('refresh', [AuthController::class, 'refreshToken']);
});
