<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('vacancy', function (Blueprint $table) {
            $table->id();
            $table->string('title', 32);
            $table->string('description', 512);
            $table->foreignId('role_id')->constrained('domestic_role')->cascadeOnDelete();
            $table->foreignId('city_id')->constrained('city')->cascadeOnDelete();
            $table->smallInteger('age_min')->nullable();
            $table->smallInteger('age_max')->nullable();
            $table->string('gender', 1);
            $table->smallInteger('experience')->nullable();
            $table->integer('wage')->nullable();
            $table->foreignId('employer_id')->constrained('employer')->cascadeOnDelete();
            $table->foreignId('schedule_id')->constrained('schedule')->cascadeOnDelete();
            $table->foreignId('additional_info_id')->constrained('additional_info')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('vacancy');
    }
};
