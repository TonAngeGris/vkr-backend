<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employer', function (Blueprint $table) {
            $table->id();
            $table->string('firstname', 50);
            $table->string('lastname', 50);
            $table->string('patronymic', 50)->nullable();
            $table->string('password', 128);
            $table->foreignId('city_id')->nullable()->constrained('city')->cascadeOnDelete();
            $table->string('phone_number', 12);
            $table->smallInteger('age');
            $table->string('gender', 1);
            $table->decimal('rating')->nullable();
            $table->string('description', 512)->nullable();
            $table->string('avatar', 128)->nullable()->default('default-male.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employer');
    }
};
