<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('resume', function (Blueprint $table) {
            $table->id();
            $table->string('title', 32);
            $table->string('description', 512);
            $table->foreignId('role_id')->constrained('domestic_role')->cascadeOnDelete();
            $table->foreignId('city_id')->constrained('city')->cascadeOnDelete();
            $table->smallInteger('experience')->nullable();
            $table->integer('wage')->nullable();
            $table->foreignId('worker_id')->constrained('worker')->cascadeOnDelete();
            $table->foreignId('schedule_id')->constrained('schedule')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('resume');
    }
};