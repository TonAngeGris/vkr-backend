<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('contract', function (Blueprint $table) {
            $table->id();
            $table->string('title', 32);
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->integer('wage')->nullable();
            $table->foreignId('role_id')->constrained('domestic_role')->cascadeOnDelete();
            $table->foreignId('city_id')->constrained('city')->cascadeOnDelete();
            $table->boolean('worker_completion_agreement')->nullable();
            $table->boolean('employer_completion_agreement')->nullable();
            $table->decimal('worker_rating')->nullable();
            $table->decimal('employer_rating')->nullable();
            $table->string('worker_comment', 512)->nullable();
            $table->string('employer_comment', 512)->nullable();
            $table->foreignId('worker_id')->constrained('worker')->cascadeOnDelete();
            $table->foreignId('employer_id')->constrained('employer')->cascadeOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('contract');
    }
};
