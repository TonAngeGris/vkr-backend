<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('citizenship_additional_info', function (Blueprint $table) {
            $table->foreignId('additional_info_id')->constrained('additional_info')->cascadeOnDelete();
            $table->foreignId('citizenship_id')->constrained('citizenship')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('citizenship_additional_info');
    }
};
