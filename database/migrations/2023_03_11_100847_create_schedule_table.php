<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('schedule', function (Blueprint $table) {
            $table->id();
            $table->foreignId('monday_id')->nullable()->constrained('day_schedule');
            $table->foreignId('tuesday_id')->nullable()->constrained('day_schedule');
            $table->foreignId('wednesday_id')->nullable()->constrained('day_schedule');
            $table->foreignId('thursday_id')->nullable()->constrained('day_schedule');
            $table->foreignId('friday_id')->nullable()->constrained('day_schedule');
            $table->foreignId('sunday_id')->nullable()->constrained('day_schedule');
            $table->foreignId('saturday_id')->nullable()->constrained('day_schedule');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('schedule');
    }
};
