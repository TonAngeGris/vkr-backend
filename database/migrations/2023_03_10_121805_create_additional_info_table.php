<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('additional_info', function (Blueprint $table) {
            $table->id();
            $table->boolean('smoking')->nullable();
            $table->boolean('foreign_passport')->nullable();
            $table->boolean('ready_for_moving')->nullable();
            $table->boolean('ready_for_traveling')->nullable();
            $table->boolean('have_children')->nullable();
            $table->boolean('driver_licence')->nullable();
            $table->boolean('nonconviction_certificate')->nullable();
            $table->boolean('medicine_certificate')->nullable();
            $table->boolean('mental_state_certificate')->nullable();
            $table->boolean('medical_tests')->nullable();
            $table->boolean('can_swimming')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('additional_info');
    }
};
