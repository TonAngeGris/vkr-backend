<?php

namespace Database\Factories;

use App\Models\AdditionalInfo;
use App\Models\City;
use App\Models\Language;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Worker>
 */
class WorkerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = \Faker\Factory::create('ru_RU');

        $lastName = $this->faker->lastName;
        $lastName = rtrim($lastName, 'а');

        $age = rand(16, 80);

        return [
            'firstname' => $this->faker->firstNameMale(),
            'lastname' => $lastName,
            'patronymic' => $faker->middleNameMale(),
            // 'password' => $this->faker->password(6, 20),
            'password' => Hash::make('123123'),
            'phone_number' => '+79' . $this->faker->randomNumber(9, true),
            'city_id' => City::inRandomOrder()->first()->id,
            'age' => $age,
            'gender' => ['М', 'Ж'][rand(0, 1)],
            'rating' => $this->faker->randomFloat(1, 0, 5),
            'total_experience' => rand(1, $age),
            'description' => $this->faker->text(512),
            'additional_info_id' => AdditionalInfo::factory(),
        ];
    }
}
