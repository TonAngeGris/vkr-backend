<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\DomesticRole;
use App\Models\Employer;
use App\Models\Worker;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Contract>
 */
class ContractFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $monthInSeconds = 60 * 60 * 24 * 30;
        $randStartTime = rand(time() - $monthInSeconds, time());
        $randStartDate = date("m.d.Y", $randStartTime);
        $randEndTime = rand($randStartTime, time());
        $randEndDate = date("m.d.Y", $randEndTime);

        $workerAgreement = rand(0, 1);
        $employerAgreement = rand(0, 1);

        $workerRating = null;
        if ($employerAgreement)
            $workerRating = rand(0, 5);

        $employerRating = null;
        if ($workerAgreement)
            $employerRating = rand(0, 5);

        return [
            'title' => $this->faker->text(32),
            'start_date' => $randStartDate,
            'end_date' => $randEndDate,
            'wage' => rand(1, 10) * 10_000,
            'role_id' => DomesticRole::inRandomOrder()->first()->id,
            'city_id' => City::inRandomOrder()->first()->id,
            'worker_completion_agreement' => $workerAgreement,
            'employer_completion_agreement' => $employerAgreement,
            'worker_rating' => $workerRating,
            'employer_rating' => $employerRating,
            'worker_comment' => $this->faker->text(32),
            'employer_comment' => $this->faker->text(32),
            'worker_id' => Worker::inRandomOrder()->first()->id,
            'employer_id' => Employer::inRandomOrder()->first()->id,
        ];
    }
}