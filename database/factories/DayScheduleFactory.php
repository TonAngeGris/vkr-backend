<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\DaySchedule>
 */
class DayScheduleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $startHour = mt_rand(0, 22);

        return [
            'start_time' => $startHour . ':00',
            'end_time' => mt_rand($startHour, 23) . ':00',
        ];
    }
}