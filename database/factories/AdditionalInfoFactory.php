<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AdditionalInfo>
 */
class AdditionalInfoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'smoking' => rand(0, 1),
            'foreign_passport' => rand(0, 1),
            'ready_for_moving' => rand(0, 1),
            'ready_for_traveling' => rand(0, 1),
            'have_children' => rand(0, 1),
            'driver_licence' => rand(0, 1),
            'nonconviction_certificate' => rand(0, 1),
            'medicine_certificate' => rand(0, 1),
            'mental_state_certificate' => rand(0, 1),
            'medical_tests' => rand(0, 1),
            'can_swimming' => rand(0, 1),
        ];
    }
}