<?php

namespace Database\Factories;

use App\Models\DaySchedule;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Schedule>
 */
class ScheduleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'monday_id' => DaySchedule::factory(),
            'tuesday_id' => DaySchedule::factory(),
            'wednesday_id' => DaySchedule::factory(),
            'thursday_id' => DaySchedule::factory(),
            'friday_id' => DaySchedule::factory(),
            'sunday_id' => DaySchedule::factory(),
            'saturday_id' => DaySchedule::factory(),
        ];
    }
}