<?php

namespace Database\Factories;

use App\Models\AdditionalInfo;
use App\Models\City;
use App\Models\DomesticRole;
use App\Models\Employer;
use App\Models\Schedule;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Vacancy>
 */
class VacancyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $ageMin = [null, rand(16, 100)][rand(0, 1)];
        $ageMax = [null, rand($ageMin, 100)][rand(0, 1)];
        $genderValues = ["М", "Ж"];

        return [
            'title' => $this->faker->text(32),
            'description' => $this->faker->text(512),
            'role_id' => DomesticRole::inRandomOrder()->first()->id,
            'city_id' => City::inRandomOrder()->first()->id,
            'age_min' => $ageMin,
            'age_max' => $ageMax,
            'gender' => $genderValues[array_rand($genderValues)],
            'experience' => rand($ageMin, $ageMax),
            'wage' => rand(1, 10) * 10_000,
            'employer_id' => Employer::inRandomOrder()->first()->id,
            'schedule_id' => Schedule::factory(),
            'additional_info_id' => AdditionalInfo::factory(),
            'created_at' => date('Y-m-d H:i:s'),
        ];
    }
}