<?php

namespace Database\Factories;

use App\Models\City;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Employer>
 */
class EmployerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $faker = \Faker\Factory::create('ru_RU');

        $lastName = $this->faker->lastName;
        $lastName = rtrim($lastName, 'а');

        return [
            'firstname' => $this->faker->firstNameMale(),
            'lastname' => $lastName,
            'patronymic' => $faker->middleNameMale(),
            'password' => $this->faker->password(6, 20),
            'phone_number' => '+79' . $this->faker->randomNumber(9, true),
            'city_id' => City::inRandomOrder()->first()->id,
            'age' => rand(16, 80),
            'gender' => ['М', 'Ж'][rand(0, 1)],
            'rating' => $this->faker->randomFloat(1, 0, 5),
            'description' => $this->faker->text(512),
        ];
    }
}