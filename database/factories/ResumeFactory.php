<?php

namespace Database\Factories;

use App\Models\City;
use App\Models\DomesticRole;
use App\Models\Schedule;
use App\Models\Worker;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Resume>
 */
class ResumeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->text(32),
            'description' => $this->faker->text(512),
            'role_id' => DomesticRole::inRandomOrder()->first()->id,
            'city_id' => City::inRandomOrder()->first()->id,
            'experience' => rand(1, 20),
            'wage' => rand(1, 10) * 10_000,
            'worker_id' => Worker::inRandomOrder()->first()->id,
            'schedule_id' => Schedule::factory(),
            'created_at' => date('Y-m-d H:i:s'),
        ];
    }
}