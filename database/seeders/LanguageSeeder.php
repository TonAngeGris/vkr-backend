<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Language::truncate();

        $csvFile = fopen(base_path('database/data/languages.csv'), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 200, ",")) !== FALSE) {
            if (!$firstline) {
                Language::create([
                    "language_name" => $data['0'],
                ]);
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
