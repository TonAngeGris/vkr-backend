<?php

namespace Database\Seeders;

use App\Models\AdditionalInfo;
use App\Models\Language;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageAdditionalInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $i = 0;
        while ($i++ !== 20) {
            DB::table('language_additional_info')->insert([
                'additional_info_id' => AdditionalInfo::inRandomOrder()->first()->id,
                'language_id' => Language::inRandomOrder()->first()->id,
            ]);
        }
    }
}
