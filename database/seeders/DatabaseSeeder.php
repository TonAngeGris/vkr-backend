<?php

namespace Database\Seeders;

use App\Models\Contract;
use App\Models\Employer;
use App\Models\Resume;
use App\Models\Vacancy;
use App\Models\Worker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $this->call([
            CitizenshipSeeder::class,
            CitySeeder::class,
            DomesticRoleSeeder::class,
            EducationSeeder::class,
            LanguageSeeder::class,
        ]);

        //! DEV
        Employer::factory()->count(20)->create();
        Worker::factory()->count(20)->create();
        Contract::factory()->count(30)->create();
        Vacancy::factory()->count(30)->create();
        Resume::factory()->count(30)->create();

        $this->call([
            LanguageAdditionalInfoSeeder::class,
            EducationAdditionalInfoSeeder::class,
            CitizenshipAdditionalInfoSeeder::class,
        ]);
    }
}