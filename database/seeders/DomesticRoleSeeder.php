<?php

namespace Database\Seeders;

use App\Models\DomesticRole;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DomesticRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DomesticRole::truncate();

        $csvFile = fopen(base_path('database/data/domestic_worker_roles.csv'), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 200, ",")) !== FALSE) {
            if (!$firstline) {
                DomesticRole::create([
                    "domestic_role_name" => $data['0'],
                ]);
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
