<?php

namespace Database\Seeders;

use App\Models\Education;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EducationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Education::truncate();

        $csvFile = fopen(base_path('database/data/education_branches.csv'), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 200, ",")) !== FALSE) {
            if (!$firstline) {
                Education::create([
                    "education_branch" => $data['0'],
                ]);
            }
            $firstline = false;
        }

        fclose($csvFile);
    }
}
