<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Schedule extends Model
{
    use HasFactory;

    protected $table = 'schedule';

    protected $fillable = [
        'monday_id',
        'tuesday_id',
        'wednesday_id',
        'thursday_id',
        'friday_id',
        'sunday_id',
        'saturday_id'
    ];

    protected $hidden = [
        'id',
        'monday_id',
        'tuesday_id',
        'wednesday_id',
        'thursday_id',
        'friday_id',
        'sunday_id',
        'saturday_id'
    ];

    public function monday(): BelongsTo
    {
        return $this->belongsTo(DaySchedule::class, 'monday_id', 'id');
    }

    public function tuesday(): BelongsTo
    {
        return $this->belongsTo(DaySchedule::class, 'tuesday_id', 'id');
    }

    public function wednesday(): BelongsTo
    {
        return $this->belongsTo(DaySchedule::class, 'wednesday_id', 'id');
    }

    public function thursday(): BelongsTo
    {
        return $this->belongsTo(DaySchedule::class, 'thursday_id', 'id');
    }

    public function friday(): BelongsTo
    {
        return $this->belongsTo(DaySchedule::class, 'friday_id', 'id');
    }

    public function sunday(): BelongsTo
    {
        return $this->belongsTo(DaySchedule::class, 'sunday_id', 'id');
    }

    public function saturday(): BelongsTo
    {
        return $this->belongsTo(DaySchedule::class, 'saturday_id', 'id');
    }

    public function detailedSchedule(): Attribute
    {
        return Attribute::make(
        get: fn($value, $attributes) => [
                $this->monday,
                $this->tuesday,
                $this->wednesday,
                $this->thursday,
                $this->friday,
                $this->sunday,
                $this->saturday,
            ]
        );
    }

    public $timestamps = false;
}
