<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class AdditionalInfo extends Model
{
    use HasFactory;

    protected $table = 'additional_info';

    protected $fillable = [
        'smoking',
        'foreign_passport',
        'ready_for_moving',
        'ready_for_traveling',
        'have_children',
        'driver_licence',
        'nonconviction_certificate',
        'medicine_certificate',
        'mental_state_certificate',
        'medical_tests',
        'can_swimming'
    ];

    public $timestamps = false;

    public function languages(): BelongsToMany
    {
        return $this
            ->belongsToMany(
                Language::class,
                'language_additional_info',
                'additional_info_id',
                'language_id'
            );
    }

    public function citizenships(): BelongsToMany
    {
        return $this
            ->belongsToMany(
                Citizenship::class,
                'citizenship_additional_info',
                'additional_info_id',
                'citizenship_id'
            );
    }

    public function educations(): BelongsToMany
    {
        return $this
            ->belongsToMany(
                Education::class,
                'education_additional_info',
                'additional_info_id',
                'education_id'
            );
    }
}
