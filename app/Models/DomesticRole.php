<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DomesticRole extends Model
{
    use HasFactory;

    protected $table = 'domestic_role';

    protected $fillable = [
        'domestic_role_name',
    ];

    public $timestamps = false;
}
