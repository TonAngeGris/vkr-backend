<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Employer extends Authenticatable
{
    use HasApiTokens, HasFactory;

    protected $table = 'employer';

    protected $fillable = [
        'firstname',
        'lastname',
        'patronymic',
        'password',
        'phone_number',
        'city_id',
        'age',
        'gender',
        'rating',
        'avatar',
        'description'
    ];

    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
        'city_id'
    ];

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function contracts(): HasMany
    {
        return $this->hasMany(Contract::class);
    }

    public function vacancies(): HasMany
    {
        return $this->hasMany(Vacancy::class);
    }

    public function findForPassport($phoneNumber): Employer
    {
        return $this->where('phone_number', $phoneNumber)->first();
    }
}
