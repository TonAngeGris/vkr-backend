<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DaySchedule extends Model
{
    use HasFactory;

    protected $table = 'day_schedule';

    protected $fillable = [
        'start_time',
        'end_time'
    ];

    protected $hidden = [
        'id',
    ];

    public $timestamps = false;
}
