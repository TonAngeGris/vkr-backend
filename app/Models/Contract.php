<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Contract extends Model
{
    use HasFactory;

    protected $table = 'contract';

    protected $fillable = [
        'title',
        'role',
        'city',
        'wage',
        'start_date',
        'end_date',
        'worker_assigned',
        'employer_assigned',
        'worker_completion_agreement',
        'employer_completion_agreement',
        'worker_rating',
        'employer_rating',
        'worker_comment',
        'employer_comment',
        'worker_id',
        'employer_id',
    ];

    protected $hidden = [
        'worker_id',
        'employer_id',
        'role_id',
        'city_id',
        'created_at',
        'updated_at',
    ];

    public function employer(): BelongsTo
    {
        return $this->belongsTo(Employer::class);
    }

    public function worker(): BelongsTo
    {
        return $this->belongsTo(Worker::class);
    }

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(DomesticRole::class);
    }
}
