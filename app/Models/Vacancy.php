<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Notifications\Notifiable;

class Vacancy extends Model
{
    use HasFactory, Notifiable;

    protected $table = 'vacancy';

    protected $fillable = [
        'title',
        'description',
        'role_id',
        'city_id',
        'age_min',
        'age_max',
        'gender',
        'experience',
        'wage',
        'employer_id',
        'schedule_id',
        'additional_info_id',
    ];

    protected $hidden = [
        'role_id',
        'city_id',
        'schedule_id',
        'updated_at',
        'additional_info_id',
    ];

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function role(): BelongsTo
    {
        return $this->belongsTo(DomesticRole::class, 'role_id', 'id');
    }

    public function employer(): BelongsTo
    {
        return $this->belongsTo(Employer::class);
    }

    public function additionalInfo(): BelongsTo
    {
        return $this->belongsTo(AdditionalInfo::class);
    }

    public function schedule(): BelongsTo
    {
        return $this->belongsTo(Schedule::class);
    }
}
