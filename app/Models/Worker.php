<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Worker extends Authenticatable
{
    use HasApiTokens, HasFactory;

    protected $table = 'worker';

    protected $fillable = [
        'firstname',
        'lastname',
        'patronymic',
        'password',
        'phone_number',
        'city_id',
        'age',
        'gender',
        'rating',
        'total_experience',
        'description',
        'avatar',
        'additional_info_id',
    ];

    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
        'city_id',
        'additional_info_id'
    ];

    public function city(): BelongsTo
    {
        return $this->belongsTo(City::class);
    }

    public function additionalInfo(): BelongsTo
    {
        return $this->belongsTo(AdditionalInfo::class);
    }

    public function contracts(): HasMany
    {
        return $this->hasMany(Contract::class);
    }

    public function resumes(): HasMany
    {
        return $this->hasMany(Resume::class);
    }

    public function findForPassport($phoneNumber): Worker
    {
        return $this->where('phone_number', $phoneNumber)->first();
    }
}
