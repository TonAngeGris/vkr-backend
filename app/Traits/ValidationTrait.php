<?php

namespace App\Traits;

trait ValidationTrait
{
    public function messages(): array
    {
        return [
            'firstName.required' => 'Необходимо ввести имя',
            'firstName.string' => 'Имя должно быть строкой',
            'firstName.max' => 'Имя должно содержать максимум 50 символов',

            'lastName.required' => 'Необходимо ввести фамилию',
            'lastName.string' => 'Фамилия должна быть строкой',
            'lastName.max' => 'Фамилия должна содержать максимум 50 символов',

            'patronymic.string' => 'Отчество должно быть строкой',
            'patronymic.max' => 'Отчество должно содержать максимум 50 символов',

            'age.required' => "Необходимо ввести возраст",
            'age.numeric' => "Возраст должен быть числом",
            'age.min' => 'Вам должно быть минимум 16 лет',
            'age.max' => 'Вам не может быть 200 лет',

            'city.required' => 'Необходимо ввести город',

            'totalExperience.required' => 'Необходимо ввести стаж',
            'totalExperience.numeric' => 'Стаж должен быть числом',
            'totalExperience.lt' => 'Стаж должен быть меньше возраста',

            'password.required' => 'Необходимо ввести пароль',
            'password.min' => 'Пароль должен содержать минимум 6 символов',
            'password.max' => 'Пароль должен содержать максимум 128 символов',

            'repeatedPassword.required' => 'Необходимо ввести повтор пароля',
            'repeatedPassword.same' => 'Пароли не совпадают',

            'phoneNumber.required' => 'Необходимо ввести номер телефона',
            'phoneNumber.unique' => 'Этот номер уже используется',
            'phoneNumber.size' => 'Неправильный формат номера',

            'gender.required' => 'Необходимо выбрать пол'
        ];
    }
}
