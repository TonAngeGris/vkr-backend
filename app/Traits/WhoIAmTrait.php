<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

trait WhoIAmTrait
{
    public function whoiam(Request $request): JsonResponse
    {
        return response()->json(auth()->user());
    }
}