<?php

namespace App\Http\Controllers;

use App\Models\Language;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class LanguageController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $languageName = $request->query('query');

        $queryResult = Language::query()
            ->where('language_name', 'ILIKE', "%$languageName%")
            ->orderBy('language_name')
            ->get()
            ->all();

        $languages = [];
        foreach ($queryResult as $item) {
            $languages[] = $item->getAttribute('language_name');
        }

        return response()->json($languages, Response::HTTP_OK);
    }
}
