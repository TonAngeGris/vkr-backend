<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function refreshToken(Request $request): JsonResponse
    {
        $userGroup = $request->userGroup;
        $refreshToken = $request->refreshToken;

        $req = Request::create('/oauth/token', 'POST', [
            'grant_type' => 'refresh_token',
            'refresh_token' => $refreshToken,
            'client_id' => config("passport.password_grant_clients.$userGroup.id"),
            'client_secret' => config("passport.password_grant_clients.$userGroup.secret"),
        ]);

        $res = app()->handle($req);
        $responseBody = json_decode($res->getContent());

        return response()->json($responseBody, $res->getStatusCode());
    }
}
