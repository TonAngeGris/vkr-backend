<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Contract;
use App\Models\DomesticRole;
use App\Models\Employer;
use App\Models\Worker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ContractController extends Controller
{
    public function store(Request $request)
    {
        $contract = Contract::make([
            'title' => $request->input('title'),
            'wage' => $request->input('wage'),
            'start_date' => Carbon::now()->format('Y-m-d'),
        ]);

        $city = City::where('city_name', $request->input('city'))->first();
        $contract->city()->associate($city);

        $role = DomesticRole::where('domestic_role_name', $request->input('role'))->first();
        $contract->role()->associate($role);

        $worker = Worker::find($request->input('workerId'));
        $contract->worker()->associate($worker);

        $employer = Employer::find($request->input('employerId'));
        $contract->employer()->associate($employer);

        if ($request->input('isWorker')) {
            $contract->fill([
                'worker_assigned' => true,
            ]);
        } elseif ($request->input('isEmployer')) {
            $contract->fill([
                'employer_assigned' => true,
            ]);
        } else {
            return response()->json([
                'error' => 'Произошла ошибка с определнием вашей роли... Повторите попытку'
            ], Response::HTTP_NOT_ACCEPTABLE);
        }

        $contract->save();

        return response(status: Response::HTTP_NO_CONTENT);
    }

    public function destroy(Request $request, Contract $contract)
    {
        $contract->delete();

        return response(status: Response::HTTP_NO_CONTENT);
    }

    public function update(Request $request, Contract $contract)
    {
        if ($request->input('employerCompletionAgreement') && !$contract->getAttribute('employer_completion_agreement')) {
            $endDate = $contract->end_date ?? Carbon::now()->format('Y-m-d');

            $contract->update([
                'employer_completion_agreement' => true,
                'employer_comment' => $request->input('comment'),
                'worker_rating' => $request->input('rating'),
                'end_date' => $endDate,
            ]);

        } elseif ($request->input('workerCompletionAgreement') && !$contract->getAttribute('worker_completion_agreement')) {
            $endDate = $contract->end_date ?? Carbon::now()->format('Y-m-d');

            $contract->update([
                'worker_completion_agreement' => true,
                'worker_comment' => $request->input('comment'),
                'employer_rating' => $request->input('rating'),
                'end_date' => $endDate,
            ]);

        } elseif ($request->input('employerAssigned')) {
            $contract->update([
                'employer_assigned' => true,
            ]);
        } elseif ($request->input('workerAssigned')) {
            $contract->update([
                'worker_assigned' => true,
            ]);
        }

        $newWorkerTotalRating = null;
        $newEmployerTotalRating = null;

        if ($contract->getAttribute('employer_completion_agreement') && $contract->getAttribute('worker_completion_agreement')) {
            // Обновление рейтинга работодателя
            $employer = Employer::find($contract->employer->id);

            $employerTotalRating = $employer->rating;
            $employerContractsCount = Contract::where('employer_id', $employer->id)
                ->where('employer_completion_agreement', true)
                ->where('worker_completion_agreement', true)
                ->count();

            $employerCurrentContractRating = $contract->getAttribute('employer_rating');

            $newEmployerTotalRating = ($employerTotalRating * ($employerContractsCount - 1) + $employerCurrentContractRating) / $employerContractsCount;

            $employer->update([
                'rating' => $newEmployerTotalRating
            ]);

            // Обновление рейтинга работника
            $worker = Worker::find($contract->worker->id);

            // Рейтинг работника по завершенным контрактам
            $workerTotalRating = $worker->rating;

            // Количество завершенных контрактов работника
            $workerContractsCount = Contract::where('worker_id', $worker->id)
                ->where('employer_completion_agreement', true)
                ->where('worker_completion_agreement', true)
                ->count();

            // Рейтинг работника по только что завершенному контракту
            $workerCurrentContractRating = $contract->getAttribute('worker_rating');

            $newWorkerTotalRating = ($workerTotalRating * ($workerContractsCount - 1) + $workerCurrentContractRating) / $workerContractsCount;

            $worker->update([
                'rating' => $newWorkerTotalRating
            ]);
        }

        $contract->city;
        $contract->role;

        $contract->worker;
        if ($newWorkerTotalRating) {
            $contract->worker->rating = $newWorkerTotalRating;
        }

        $contract->employer;
        if ($newWorkerTotalRating) {
            $contract->employer->rating = $newEmployerTotalRating;
        }

        return response()->json($contract, Response::HTTP_OK);
    }
}
