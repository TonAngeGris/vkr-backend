<?php

namespace App\Http\Controllers;

use App\Models\AdditionalInfo;
use App\Models\City;
use App\Models\DaySchedule;
use App\Models\DomesticRole;
use App\Models\Employer;
use App\Models\Schedule;
use App\Models\Vacancy;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class VacancyController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $limit = 5;
        $offset = $request->query('page') - 1 ?? 0;
        $showingAmount = $offset * $limit;

        $query = Vacancy::with([
            'city:id,city_name',
            'role:id,domestic_role_name',
            'schedule.monday',
            'schedule.tuesday',
            'schedule.wednesday',
            'schedule.thursday',
            'schedule.friday',
            'schedule.sunday',
            'schedule.saturday',
            'additionalInfo',
            'employer:id,firstname,lastname,rating',
        ])
            ->select('vacancy.*', 'rating as employerRating', 'city_name as vacancyCity')
            ->where(function (Builder $query) use ($request) {
                $query->where('vacancy.title', 'ILIKE', "%" . $request->query('searchText') . "%")
                    ->orWhere('vacancy.description', 'ILIKE', "%" . $request->query('searchText') . "%");
            })
            ->whereRelation('role', function (Builder $query) use ($request) {
                if ($request->query('role') !== 'Все специальности') {
                    $query->where('domestic_role_name', 'LIKE', $request->query('role') ?? '%');
                }
            })
            ->whereRelation('city', 'city_name', 'LIKE', '%' . $request->query('city') . '%' ?? '%')
            ->where(function (Builder $query) use ($request) {
                $query->whereNull('age_min')->orWhere('age_min', '<=', $request->query('ageMin') ?? 999);
            })
            ->where(function (Builder $query) use ($request) {
                $query->whereNull('age_max')->orWhere('age_max', '>=', $request->query('ageMax') ?? 0);
            })
            ->where(function (Builder $query) use ($request) {
                $query->whereNull('experience')->orWhere('experience', '>=', $request->query('experience') ?? 0);
            })
            ->where(function (Builder $query) use ($request) {
                $query->whereNull('wage')->orWhere('wage', '<=', $request->query('wageMax') ?? 1_000_000_000);
            })
            ->where(function (Builder $query) use ($request) {
                $query->whereNull('wage')->orWhere('wage', '>=', $request->query('wageMin') ?? 0);
            })
            ->when($request->query('mondayStartTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.monday', 'start_time', '>=', Carbon::parse($request->query('mondayStartTime')));
            })
            ->when($request->query('mondayEndTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.monday', 'end_time', '<=', Carbon::parse($request->query('mondayEndTime') ?? '23:59'));
            })
            ->when($request->query('tuesdayStartTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.tuesday', 'start_time', '>=', Carbon::parse($request->query('tuesdayStartTime')));
            })
            ->when($request->query('tuesdayEndTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.tuesday', 'end_time', '<=', Carbon::parse($request->query('tuesdayEndTime') ?? '23:59'));
            })
            ->when($request->query('wednesdayStartTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.wednesday', 'start_time', '>=', Carbon::parse($request->query('wednesdayStartTime')));
            })
            ->when($request->query('wednesdayEndTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.wednesday', 'end_time', '<=', Carbon::parse($request->query('wednesdayEndTime') ?? '23:59'));
            })
            ->when($request->query('thursdayStartTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.thursday', 'start_time', '>=', Carbon::parse($request->query('thursdayStartTime')));
            })
            ->when($request->query('thursdayEndTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.thursday', 'end_time', '<=', Carbon::parse($request->query('thursdayEndTime') ?? '23:59'));
            })
            ->when($request->query('fridayStartTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.friday', 'start_time', '>=', Carbon::parse($request->query('fridayStartTime')));
            })
            ->when($request->query('fridayEndTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.friday', 'end_time', '<=', Carbon::parse($request->query('fridayEndTime') ?? '23:59'));
            })
            ->when($request->query('sundayStartTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.sunday', 'start_time', '>=', Carbon::parse($request->query('sundayStartTime')));
            })
            ->when($request->query('sundayEndTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.sunday', 'end_time', '<=', Carbon::parse($request->query('sundayEndTime') ?? '23:59'));
            })
            ->when($request->query('saturdayStartTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.saturday', 'start_time', '>=', Carbon::parse($request->query('saturdayStartTime')));
            })
            ->when($request->query('saturdayEndTime'), function ($query) use ($request) {
                return $query->whereRelation('schedule.saturday', 'end_time', '<=', Carbon::parse($request->query('saturdayEndTime') ?? '23:59'));
            })
            ->join('employer', 'employer.id', '=', 'vacancy.employer_id')
            ->join('city', 'city.id', '=', 'vacancy.city_id');

        switch ($request->query('sort')) {
            case 'city':
                $query->orderBy('city.city_name');
                break;
            case 'experience':
                $query->orderByDesc('experience');
                break;
            case 'wage':
                $query->orderByDesc('wage');
                break;
            case 'rating':
                $query->orderByRaw('-employer.rating ASC');
                break;
            default:
                $query->orderByDesc('created_at');
                break;
        }

        $vacancyAmount = $query->count();
        $vacancies = $query->offset($showingAmount)->limit($limit)->get();

        return response()->json([
            'vacancies' => $vacancies,
            'vacancyAmount' => $vacancyAmount
        ], 200);
    }

    public function show(Vacancy $vacancy): JsonResponse
    {
        $vacancyData = Vacancy::with([
            'city:id,city_name',
            'role:id,domestic_role_name',
            'schedule.monday',
            'schedule.tuesday',
            'schedule.wednesday',
            'schedule.thursday',
            'schedule.friday',
            'schedule.sunday',
            'schedule.saturday',
            'additionalInfo'
        ])->find($vacancy->id);

        return response()->json($vacancyData, 200);
    }

    public function store(Request $request): JsonResponse
    {
        $vacancy = Vacancy::make(array_filter([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'experience' => $request->input('experience'),
            'wage' => $request->input('wage'),
            'age_min' => $request->input('ageMin'),
            'age_max' => $request->input('ageMax'),
        ]));

        $additionalInfo = null;
        if ($request->input('additionalInfo')) {
            $additionalInfoData = $request->input('additionalInfo');

            $additionalInfo = AdditionalInfo::create([
                'smoking' => $additionalInfoData['smoking'],
                'foreign_passport' => $additionalInfoData['foreignPassport'],
                'ready_for_moving' => $additionalInfoData['readyForMoving'],
                'ready_for_traveling' => $additionalInfoData['readyForTraveling'],
                'have_children' => $additionalInfoData['haveChildren'],
                'driver_licence' => $additionalInfoData['driverLicence'],
                'nonconviction_certificate' => $additionalInfoData['nonconvictionCertificate'],
                'medicine_certificate' => $additionalInfoData['medicineCertificate'],
                'mental_state_certificate' => $additionalInfoData['mentalStateCertificate'],
                'medical_tests' => $additionalInfoData['medicalTests'],
                'can_swimming' => $additionalInfoData['canSwimming'],
            ]);
        }
        $vacancy->additionalInfo()->associate($additionalInfo);

        $employer = Employer::find($request->input('employerId'));
        $vacancy->employer()->associate($employer);

        $city = City::where('city_name', $request->input('city'))->first();
        $vacancy->city()->associate($city);

        $role = DomesticRole::where('domestic_role_name', $request->input('role'))->first();
        $vacancy->role()->associate($role);

        $monday = null;
        if ($request->input('mondayStartTime') && $request->input('mondayEndTime')) {
            $monday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('mondayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('mondayEndTime'))->format('H:i:s'),
            ]);
        }

        $tuesday = null;
        if ($request->input('tuesdayStartTime') && $request->input('tuesdayEndTime')) {
            $tuesday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('tuesdayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('tuesdayEndTime'))->format('H:i:s'),
            ]);
        }

        $wednesday = null;
        if ($request->input('wednesdayStartTime') && $request->input('wednesdayEndTime')) {
            $wednesday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('wednesdayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('wednesdayEndTime'))->format('H:i:s'),
            ]);
        }

        $thursday = null;
        if ($request->input('thursdayStartTime') && $request->input('thursdayEndTime')) {
            $thursday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('thursdayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('thursdayEndTime'))->format('H:i:s'),
            ]);
        }

        $friday = null;
        if ($request->input('fridayStartTime') && $request->input('fridayEndTime')) {
            $friday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('fridayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('fridayEndTime'))->format('H:i:s'),
            ]);
        }

        $sunday = null;
        if ($request->input('sundayStartTime') && $request->input('sundayEndTime')) {
            $sunday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('sundayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('sundayEndTime'))->format('H:i:s'),
            ]);
        }

        $saturday = null;
        if ($request->input('saturdayStartTime') && $request->input('saturdayEndTime')) {
            $saturday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('saturdayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('saturdayEndTime'))->format('H:i:s'),
            ]);
        }

        $schedule = Schedule::make();
        $schedule->monday()->associate($monday);
        $schedule->tuesday()->associate($tuesday);
        $schedule->wednesday()->associate($wednesday);
        $schedule->thursday()->associate($thursday);
        $schedule->friday()->associate($friday);
        $schedule->sunday()->associate($sunday);
        $schedule->saturday()->associate($saturday);
        $schedule->save();

        $vacancy->schedule()->associate($schedule);
        $vacancy->save();

        unset($vacancy->employer);

        return response()->json($vacancy, Response::HTTP_CREATED);
    }

    public function update(Request $request, Vacancy $vacancy)
    {
        $vacancy->fill(array_filter([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'experience' => $request->input('experience'),
            'wage' => $request->input('wage'),
            'age_min' => $request->input('ageMin'),
            'age_max' => $request->input('ageMax'),
        ]));

        if ($request->input('additionalInfo') !== null) {
            $additionalInfoData = $request->input('additionalInfo');

            $vacancy->additionalInfo->update([
                'smoking' => $additionalInfoData['smoking'],
                'foreign_passport' => $additionalInfoData['foreignPassport'],
                'ready_for_moving' => $additionalInfoData['readyForMoving'],
                'ready_for_traveling' => $additionalInfoData['readyForTraveling'],
                'have_children' => $additionalInfoData['haveChildren'],
                'driver_licence' => $additionalInfoData['driverLicence'],
                'nonconviction_certificate' => $additionalInfoData['nonconvictionCertificate'],
                'medicine_certificate' => $additionalInfoData['medicineCertificate'],
                'mental_state_certificate' => $additionalInfoData['mentalStateCertificate'],
                'medical_tests' => $additionalInfoData['medicalTests'],
                'can_swimming' => $additionalInfoData['canSwimming'],
            ]);
        }

        if ($vacancy->city->city_name !== $request->input('city')) {
            $vacancy->city()->dissociate();

            $city = City::where('city_name', $request->input('city'))->first();
            $vacancy->city()->associate($city);
        }

        if ($vacancy->role->domestic_role_name !== $request->input('role')) {
            $vacancy->role()->dissociate();

            $role = DomesticRole::where('domestic_role_name', $request->input('role'))->first();
            $vacancy->role()->associate($role);
        }

        if ($request->input('mondayStartTime') !== null && $request->input('mondayEndTime') !== null) {
            $startTime = $request->input('mondayStartTime');
            $endTime = $request->input('mondayEndTime');

            if ($vacancy->schedule->monday) {
                $vacancy->schedule->monday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $vacancy->schedule->monday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $vacancy->schedule->monday();
            $vacancy->schedule->monday()->dissociate();
            $vacancy->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('tuesdayStartTime') !== null && $request->input('tuesdayEndTime') !== null) {
            $startTime = $request->input('tuesdayStartTime');
            $endTime = $request->input('tuesdayEndTime');

            if ($vacancy->schedule->tuesday) {
                $vacancy->schedule->tuesday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $vacancy->schedule->tuesday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $vacancy->schedule->tuesday();
            $vacancy->schedule->tuesday()->dissociate();
            $vacancy->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('wednesdayStartTime') !== null && $request->input('wednesdayEndTime') !== null) {
            $startTime = $request->input('wednesdayStartTime');
            $endTime = $request->input('wednesdayEndTime');

            if ($vacancy->schedule->wednesday) {
                $vacancy->schedule->wednesday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $vacancy->schedule->wednesday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $vacancy->schedule->wednesday();
            $vacancy->schedule->wednesday()->dissociate();
            $vacancy->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('thursdayStartTime') !== null && $request->input('thursdayEndTime') !== null) {
            $startTime = $request->input('thursdayStartTime');
            $endTime = $request->input('thursdayEndTime');

            if ($vacancy->schedule->thursday) {
                $vacancy->schedule->thursday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $vacancy->schedule->thursday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $vacancy->schedule->thursday();
            $vacancy->schedule->thursday()->dissociate();
            $vacancy->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('fridayStartTime') !== null && $request->input('fridayEndTime') !== null) {
            $startTime = $request->input('fridayStartTime');
            $endTime = $request->input('fridayEndTime');

            if ($vacancy->schedule->friday) {
                $vacancy->schedule->friday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $vacancy->schedule->friday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $vacancy->schedule->friday();
            $vacancy->schedule->friday()->dissociate();
            $vacancy->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('sundayStartTime') !== null && $request->input('sundayEndTime') !== null) {
            $startTime = $request->input('sundayStartTime');
            $endTime = $request->input('sundayEndTime');

            if ($vacancy->schedule->sunday) {
                $vacancy->schedule->sunday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $vacancy->schedule->sunday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $vacancy->schedule->sunday();
            $vacancy->schedule->sunday()->dissociate();
            $vacancy->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('saturdayStartTime') !== null && $request->input('saturdayEndTime') !== null) {
            $startTime = $request->input('saturdayStartTime');
            $endTime = $request->input('saturdayEndTime');

            if ($vacancy->schedule->saturday) {
                $vacancy->schedule->saturday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $vacancy->schedule->saturday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $vacancy->schedule->saturday();
            $vacancy->schedule->saturday()->dissociate();
            $vacancy->schedule->save();

            $dayScheduleToDelete->delete();
        }

        $vacancy->schedule->save();
        $vacancy->save();

        return response()->json($vacancy, Response::HTTP_OK);
    }

    public function destroy(Request $request, Vacancy $vacancy)
    {
        $vacancy->delete();

        return response(status: Response::HTTP_NO_CONTENT);
    }
}
