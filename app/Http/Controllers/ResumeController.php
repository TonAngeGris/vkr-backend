<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\DaySchedule;
use App\Models\DomesticRole;
use App\Models\Resume;
use App\Models\Schedule;
use App\Models\Worker;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ResumeController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $limit = 5;
        $offset = $request->query('page') - 1 ?? 0;
        $showingAmount = $offset * $limit;

        $query = Resume::with([
            'city:id,city_name',
            'role:id,domestic_role_name',
            'schedule.monday',
            'schedule.tuesday',
            'schedule.wednesday',
            'schedule.thursday',
            'schedule.friday',
            'schedule.sunday',
            'schedule.saturday',
            'worker:id,firstname,lastname,rating,age'
        ])
            ->select('resume.*', 'rating as workerRating', 'city_name as resumeCity')
            ->where(function (Builder $query) use ($request) {
                $query->where('resume.title', 'ILIKE', "%" . $request->query('searchText') . "%")
                    ->orWhere('resume.description', 'ILIKE', "%" . $request->query('searchText') . "%");
            })
            ->whereRelation('role', function (Builder $query) use ($request) {
                if ($request->query('role') !== 'Все специальности') {
                    $query->where('domestic_role_name', 'LIKE', $request->query('role') ?? '%');
                }
            })
            ->whereRelation('city', 'city_name', 'LIKE', '%' . $request->query('city') . '%' ?? '%')
            ->whereRelation('worker', function (Builder $query) use ($request) {
                $query->whereNull('age')->orWhere('age', '<=', $request->query('ageMax') ?? 999);
            })
            ->whereRelation('worker', function (Builder $query) use ($request) {
                $query->whereNull('age')->orWhere('age', '>=', $request->query('ageMin') ?? 0);
            })
            ->where(function (Builder $query) use ($request) {
                $query->whereNull('experience')->orWhere('experience', '>=', $request->query('experience') ?? 0);
            })
            ->where(function (Builder $query) use ($request) {
                $query->whereNull('wage')->orWhere('wage', '<=', $request->query('wageMax') ?? 1_000_000_000);
            })
            ->where(function (Builder $query) use ($request) {
                $query->whereNull('wage')->orWhere('wage', '>=', $request->query('wageMin') ?? 0);
            })
            ->whereRelation('schedule.monday', 'start_time', '>=', Carbon::parse($request->query('mondayStartTime') ?? '00:00'))
            ->whereRelation('schedule.monday', 'end_time', '<=', Carbon::parse($request->query('mondayEndTime') ?? '23:59'))
            ->whereRelation('schedule.tuesday', 'start_time', '>=', Carbon::parse($request->query('tuesdayStartTime') ?? '00:00'))
            ->whereRelation('schedule.tuesday', 'end_time', '<=', Carbon::parse($request->query('tuesdayEndTime') ?? '23:59'))
            ->whereRelation('schedule.wednesday', 'start_time', '>=', Carbon::parse($request->query('wednesdayStartTime') ?? '00:00'))
            ->whereRelation('schedule.wednesday', 'end_time', '<=', Carbon::parse($request->query('wednesdayEndTime') ?? '23:59'))
            ->whereRelation('schedule.thursday', 'start_time', '>=', Carbon::parse($request->query('thursdayStartTime') ?? '00:00'))
            ->whereRelation('schedule.thursday', 'end_time', '<=', Carbon::parse($request->query('thursdayEndTime') ?? '23:59'))
            ->whereRelation('schedule.friday', 'start_time', '>=', Carbon::parse($request->query('fridayStartTime') ?? '00:00'))
            ->whereRelation('schedule.friday', 'end_time', '<=', Carbon::parse($request->query('fridayEndTime') ?? '23:59'))
            ->whereRelation('schedule.sunday', 'start_time', '>=', Carbon::parse($request->query('sundayStartTime') ?? '00:00'))
            ->whereRelation('schedule.sunday', 'end_time', '<=', Carbon::parse($request->query('sundayEndTime') ?? '23:59'))
            ->whereRelation('schedule.saturday', 'start_time', '>=', Carbon::parse($request->query('saturdayStartTime') ?? '00:00'))
            ->whereRelation('schedule.saturday', 'end_time', '<=', Carbon::parse($request->query('saturdayEndTime') ?? '23:59'))
            ->join('worker', 'worker.id', '=', 'resume.worker_id')
            ->join('city', 'city.id', '=', 'resume.city_id');

        switch ($request->query('sort')) {
            case 'city':
                $query->orderBy('city.city_name');
                break;
            case 'experience':
                $query->orderByDesc('experience');
                break;
            case 'wage':
                $query->orderByDesc('wage');
                break;
            case 'rating':
                $query->orderByRaw('-worker.rating ASC');
                break;
            default:
                $query->orderByDesc('created_at');
                break;
        }

        $resumeAmount = $query->count();
        $resumes = $query->offset($showingAmount)->limit($limit)->get();

        return response()->json([
            'resumes' => $resumes,
            'resumeAmount' => $resumeAmount
        ], Response::HTTP_OK);
    }

    public function show(Resume $resume): JsonResponse
    {
        $resumeData = Resume::with([
            'role:id,domestic_role_name',
            'city:id,city_name',
            'schedule.monday',
            'schedule.tuesday',
            'schedule.wednesday',
            'schedule.thursday',
            'schedule.friday',
            'schedule.sunday',
            'schedule.saturday',
        ])->find($resume->id);

        return response()->json($resumeData, Response::HTTP_OK);
    }

    public function store(Request $request): JsonResponse
    {
        $resume = Resume::make(array_filter([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'experience' => $request->input('experience'),
            'wage' => $request->input('wage'),
        ]));

        $worker = Worker::find($request->input('workerId'));
        $resume->worker()->associate($worker);

        $city = City::where('city_name', $request->input('city'))->first();
        $resume->city()->associate($city);

        $role = DomesticRole::where('domestic_role_name', $request->input('role'))->first();
        $resume->role()->associate($role);

        $monday = null;
        if ($request->input('mondayStartTime') && $request->input('mondayEndTime')) {
            $monday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('mondayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('mondayEndTime'))->format('H:i:s'),
            ]);
        }

        $tuesday = null;
        if ($request->input('tuesdayStartTime') && $request->input('tuesdayEndTime')) {
            $tuesday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('tuesdayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('tuesdayEndTime'))->format('H:i:s'),
            ]);
        }

        $wednesday = null;
        if ($request->input('wednesdayStartTime') && $request->input('wednesdayEndTime')) {
            $wednesday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('wednesdayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('wednesdayEndTime'))->format('H:i:s'),
            ]);
        }

        $thursday = null;
        if ($request->input('thursdayStartTime') && $request->input('thursdayEndTime')) {
            $thursday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('thursdayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('thursdayEndTime'))->format('H:i:s'),
            ]);
        }

        $friday = null;
        if ($request->input('fridayStartTime') && $request->input('fridayEndTime')) {
            $friday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('fridayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('fridayEndTime'))->format('H:i:s'),
            ]);
        }

        $sunday = null;
        if ($request->input('sundayStartTime') && $request->input('sundayEndTime')) {
            $sunday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('sundayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('sundayEndTime'))->format('H:i:s'),
            ]);
        }

        $saturday = null;
        if ($request->input('saturdayStartTime') && $request->input('saturdayEndTime')) {
            $saturday = DaySchedule::create([
                'start_time' => Carbon::createFromFormat('H', $request->input('saturdayStartTime'))->format('H:i:s'),
                'end_time' => Carbon::createFromFormat('H', $request->input('saturdayEndTime'))->format('H:i:s'),
            ]);
        }

        $schedule = Schedule::make();
        $schedule->monday()->associate($monday);
        $schedule->tuesday()->associate($tuesday);
        $schedule->wednesday()->associate($wednesday);
        $schedule->thursday()->associate($thursday);
        $schedule->friday()->associate($friday);
        $schedule->sunday()->associate($sunday);
        $schedule->saturday()->associate($saturday);
        $schedule->save();

        $resume->schedule()->associate($schedule);
        $resume->save();

        return response()->json($resume, Response::HTTP_CREATED);
    }

    public function update(Request $request, Resume $resume): JsonResponse
    {
        $resume->fill(array_filter([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'experience' => $request->input('experience'),
            'wage' => $request->input('wage'),
        ]));

        if ($resume->city->city_name !== $request->input('city')) {
            $resume->city()->dissociate();

            $city = City::where('city_name', $request->input('city'))->first();
            $resume->city()->associate($city);
        }

        if ($resume->role->domestic_role_name !== $request->input('role')) {
            $resume->role()->dissociate();

            $role = DomesticRole::where('domestic_role_name', $request->input('role'))->first();
            $resume->role()->associate($role);
        }

        if ($request->input('mondayStartTime') !== null && $request->input('mondayEndTime') !== null) {
            $startTime = $request->input('mondayStartTime');
            $endTime = $request->input('mondayEndTime');

            if ($resume->schedule->monday) {
                $resume->schedule->monday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $resume->schedule->monday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $resume->schedule->monday();
            $resume->schedule->monday()->dissociate();
            $resume->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('tuesdayStartTime') !== null && $request->input('tuesdayEndTime') !== null) {
            $startTime = $request->input('tuesdayStartTime');
            $endTime = $request->input('tuesdayEndTime');

            if ($resume->schedule->tuesday) {
                $resume->schedule->tuesday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $resume->schedule->tuesday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $resume->schedule->tuesday();
            $resume->schedule->tuesday()->dissociate();
            $resume->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('wednesdayStartTime') !== null && $request->input('wednesdayEndTime') !== null) {
            $startTime = $request->input('wednesdayStartTime');
            $endTime = $request->input('wednesdayEndTime');

            if ($resume->schedule->wednesday) {
                $resume->schedule->wednesday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $resume->schedule->wednesday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $resume->schedule->wednesday();
            $resume->schedule->wednesday()->dissociate();
            $resume->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('thursdayStartTime') !== null && $request->input('thursdayEndTime') !== null) {
            $startTime = $request->input('thursdayStartTime');
            $endTime = $request->input('thursdayEndTime');

            if ($resume->schedule->thursday) {
                $resume->schedule->thursday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $resume->schedule->thursday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $resume->schedule->thursday();
            $resume->schedule->thursday()->dissociate();
            $resume->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('fridayStartTime') !== null && $request->input('fridayEndTime') !== null) {
            $startTime = $request->input('fridayStartTime');
            $endTime = $request->input('fridayEndTime');

            if ($resume->schedule->friday) {
                $resume->schedule->friday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $resume->schedule->friday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $resume->schedule->friday();
            $resume->schedule->friday()->dissociate();
            $resume->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('sundayStartTime') !== null && $request->input('sundayEndTime') !== null) {
            $startTime = $request->input('sundayStartTime');
            $endTime = $request->input('sundayEndTime');

            if ($resume->schedule->sunday) {
                $resume->schedule->sunday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $resume->schedule->sunday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $resume->schedule->sunday();
            $resume->schedule->sunday()->dissociate();
            $resume->schedule->save();

            $dayScheduleToDelete->delete();
        }

        if ($request->input('saturdayStartTime') !== null && $request->input('saturdayEndTime') !== null) {
            $startTime = $request->input('saturdayStartTime');
            $endTime = $request->input('saturdayEndTime');

            if ($resume->schedule->saturday) {
                $resume->schedule->saturday->update(
                    [
                        'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                        'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s')
                    ]
                );
            } else {
                $daySchedule = DaySchedule::create([
                    'start_time' => Carbon::createFromFormat('H', $startTime)->format('H:i:s'),
                    'end_time' => Carbon::createFromFormat('H', $endTime)->format('H:i:s'),
                ]);

                $resume->schedule->saturday()->associate($daySchedule);
            }
        } else {
            $dayScheduleToDelete = $resume->schedule->saturday();
            $resume->schedule->saturday()->dissociate();
            $resume->schedule->save();

            $dayScheduleToDelete->delete();
        }

        $resume->schedule->save();
        $resume->save();

        return response()->json($resume, Response::HTTP_OK);
    }

    public function destroy(Request $request, Resume $resume)
    {
        $resume->delete();

        return response(status: Response::HTTP_NO_CONTENT);
    }
}
