<?php

namespace App\Http\Controllers;

use App\Models\Education;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EducationController extends Controller
{
    public function index(Request $request)
    {
        $educationName = $request->query('query');

        $queryResult = Education::query()
            ->where('education_branch', 'ILIKE', "%$educationName%")
            ->orderBy('education_branch')
            ->get()
            ->all();

        $educations = [];
        foreach ($queryResult as $item) {
            $educations[] = $item->getAttribute('education_branch');
        }

        return response()->json($educations, Response::HTTP_OK);
    }
}
