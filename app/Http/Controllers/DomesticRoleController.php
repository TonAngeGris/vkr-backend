<?php

namespace App\Http\Controllers;

use App\Models\DomesticRole;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DomesticRoleController extends Controller
{
    public function index(Request $request)
    {
        $roleName = $request->query('query');

        $queryResult = DomesticRole::query()
            ->where('domestic_role_name', 'ILIKE', "%$roleName%")
            ->orderBy('domestic_role_name')
            ->get()
            ->all();

        $roles = [];
        foreach ($queryResult as $item) {
            $roles[] = $item->getAttribute('domestic_role_name');
        }

        return response()->json($roles, Response::HTTP_OK);
    }
}
