<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterEmployerRequest;
use App\Models\City;
use App\Models\Contract;
use App\Models\Employer;
use App\Models\Vacancy;
use App\Traits\WhoIAmTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class EmployerController extends Controller
{
    use WhoIAmTrait;

    public function registration(RegisterEmployerRequest $request): JsonResponse
    {
        $employer = Employer::make([
            'firstname' => $request->firstName,
            'lastname' => $request->lastName,
            'patronymic' => $request->patronymic,
            'password' => Hash::make($request->password),
            'phone_number' => $request->phoneNumber,
            'age' => $request->age,
            'gender' => $request->gender,
        ]);

        $city = City::where('city_name', $request->city)->first();
        $employer->city()->associate($city);
        $employer->save();

        $req = Request::create('/oauth/token', 'POST', [
            'grant_type' => 'password',
            'client_id' => config('passport.password_grant_clients.employers.id'),
            'client_secret' => config('passport.password_grant_clients.employers.secret'),
            'username' => $request->phoneNumber,
            'password' => $request->password,
            'provider' => 'employers'
        ]);

        $res = app()->handle($req);
        $responseBody = json_decode($res->getContent());

        return response()->json($responseBody, $res->getStatusCode());
    }

    public function login(LoginRequest $request): JsonResponse
    {
        $user = Employer::where('phone_number', $request->phoneNumber)->first();
        if (!$user) {
            return response()->json(['error' => 'Такого пользователя не существует'], Response::HTTP_BAD_REQUEST);
        }

        if (Hash::check($request->password, $user->password)) {
            $req = Request::create('/oauth/token', 'POST', [
                'grant_type' => 'password',
                'client_id' => config('passport.password_grant_clients.employers.id'),
                'client_secret' => config('passport.password_grant_clients.employers.secret'),
                'username' => $request->phoneNumber,
                'password' => $request->password,
                'provider' => 'employers'
            ]);

            $res = app()->handle($req);
            $responseBody = json_decode($res->getContent());

            return response()->json($responseBody, $res->getStatusCode());
        }

        return response()->json(['error' => 'Неправильный логин или пароль'], Response::HTTP_BAD_REQUEST);
    }

    public function showEmployerData(Employer $employer): JsonResponse
    {
        $employerData = Employer::with('city:id,city_name')->find($employer->id);

        return response()->json($employerData, Response::HTTP_OK);
    }

    public function showEmployerContracts(Employer $employer): JsonResponse
    {
        $contracts = Contract::with(
            [
                'employer:id,lastname,firstname',
                'worker:id,lastname,firstname',
                'city:id,city_name',
                'role:id,domestic_role_name'
            ]
        )
            ->where('employer_id', $employer->id)
            ->get()
            ->all();

        $contractsPendingAssignment = [];
        $contractsCompleted = [];
        $contractsInProgress = [];
        $contractsPendingCompletion = [];
        foreach ($contracts as $contract) {
            $workerAssigned = $contract->getAttribute('worker_assigned');
            $employerAssigned = $contract->getAttribute('employer_assigned');
            $workerAgreement = $contract->getAttribute('worker_completion_agreement');
            $employerAgreement = $contract->getAttribute('employer_completion_agreement');

            if ($workerAgreement && $employerAgreement) {
                $contractsCompleted[] = $contract;
            } elseif ($workerAgreement || $employerAgreement) {
                $contractsPendingCompletion[] = $contract;
            } elseif ($workerAssigned && $employerAssigned) {
                $contractsInProgress[] = $contract;
            } else {
                $contractsPendingAssignment[] = $contract;
            }
        }

        $sortedContracts = [
            'pending_assignment' => $contractsPendingAssignment,
            'in_progress' => $contractsInProgress,
            'completed' => $contractsCompleted,
            'pending_completion' => $contractsPendingCompletion,
        ];

        return response()->json($sortedContracts, Response::HTTP_OK);
    }

    public function showEmployerVacancies(Employer $employer): JsonResponse
    {
        $vacancies = Vacancy::with(
            [
                'additionalInfo',
                'city:id,city_name',
                'role:id,domestic_role_name',
                'schedule.monday',
                'schedule.tuesday',
                'schedule.wednesday',
                'schedule.thursday',
                'schedule.friday',
                'schedule.sunday',
                'schedule.saturday',
            ]
        )
            ->where('employer_id', $employer->id)
            ->get()
            ->all();

        return response()->json($vacancies, Response::HTTP_OK);
    }

    public function update(Request $request, Employer $employer): JsonResponse
    {
        $updatedEmployer = $employer->fill(array_filter([
            'firstname' => $request->input('firstName'),
            'lastname' => $request->input('lastName'),
            'partonymic' => $request->input('patronymic'),
            'phone_number' => $request->input('phoneNumber'),
            'description' => $request->input('description'),
            'gender' => $request->input('gender'),
            'age' => $request->input('age'),
            'password' => Hash::make($request->input(('password')))
        ]));

        if ($request->input('city')) {
            $city = City::where('city_name', $request->input('city'))->first();
            $employer->city()->associate($city);
        }

        if ($request->file('avatar')) {
            $file = $request->file('avatar');
            $fileName = time() . '.' . $file->getClientOriginalExtension();

            $file->storeAs('public/avatars', $fileName);

            $updatedEmployer = $employer->fill(['avatar' => $fileName]);
        }

        $employer->save();

        $employer->city;

        return response()->json($updatedEmployer, Response::HTTP_OK);
    }
}
