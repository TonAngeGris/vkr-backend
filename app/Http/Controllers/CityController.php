<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CityController extends Controller
{
    public function index(Request $request)
    {
        $cityName = $request->query('query');

        $queryResult = City::query()
            ->where('city_name', 'ILIKE', "%$cityName%")
            ->orderBy('city_name')
            ->get()
            ->all();

        $cities = [];
        foreach ($queryResult as $item) {
            $cities[] = $item->getAttribute('city_name');
        }

        return response()->json($cities, Response::HTTP_OK);
    }
}
