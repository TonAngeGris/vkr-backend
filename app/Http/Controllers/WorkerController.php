<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterWorkerRequest;
use App\Models\AdditionalInfo;
use App\Models\Citizenship;
use App\Models\City;
use App\Models\Contract;
use App\Models\Education;
use App\Models\Language;
use App\Models\Resume;
use App\Models\Worker;
use App\Traits\WhoIAmTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class WorkerController extends Controller
{
    use WhoIAmTrait;

    public function registration(RegisterWorkerRequest $request): JsonResponse
    {
        $worker = Worker::make([
            'firstname' => $request->firstName,
            'lastname' => $request->lastName,
            'patronymic' => $request->patronymic,
            'password' => Hash::make($request->password),
            'phone_number' => $request->phoneNumber,
            'total_experience' => $request->totalExperience,
            'age' => $request->age,
            'gender' => $request->gender,
        ]);

        $city = City::where('city_name', $request->city)->first();
        $worker->city()->associate($city);
        $worker->save();

        $req = Request::create('/oauth/token', 'POST', [
            'grant_type' => 'password',
            'client_id' => config('passport.password_grant_clients.workers.id'),
            'client_secret' => config('passport.password_grant_clients.workers.secret'),
            'username' => $request->phoneNumber,
            'password' => $request->password,
        ]);

        $res = app()->handle($req);
        $responseBody = json_decode($res->getContent());

        return response()->json($responseBody, $res->getStatusCode());
    }

    public function login(LoginRequest $request): JsonResponse
    {
        $user = Worker::where('phone_number', $request->phoneNumber)->first();
        if (!$user) {
            return response()->json(['error' => 'Такого пользователя не существует'], Response::HTTP_BAD_REQUEST);
        }

        if (Hash::check($request->password, $user->password)) {
            $req = Request::create('/oauth/token', 'POST', [
                'grant_type' => 'password',
                'client_id' => config('passport.password_grant_clients.workers.id'),
                'client_secret' => config('passport.password_grant_clients.workers.secret'),
                'username' => $request->phoneNumber,
                'password' => $request->password,
            ]);

            $res = app()->handle($req);
            $responseBody = json_decode($res->getContent());

            return response()->json($responseBody, $res->getStatusCode());
        }

        return response()->json(['error' => 'Неправильный логин или пароль'], Response::HTTP_BAD_REQUEST);
    }

    public function showWorkerData(Worker $worker): JsonResponse
    {
        $workerData = Worker::with([
            'city:id,city_name',
            'additionalInfo',
            'additionalInfo.citizenships',
            'additionalInfo.languages',
            'additionalInfo.educations',
        ])->find($worker->id);

        return response()->json($workerData, Response::HTTP_OK);
    }

    public function showWorkerContracts(Worker $worker): JsonResponse
    {
        $contracts = Contract::with(
            [
                'employer:id,lastname,firstname',
                'worker:id,lastname,firstname',
                'city:id,city_name',
                'role:id,domestic_role_name'
            ]
        )
            ->where('worker_id', $worker->id)
            ->get()
            ->all();

        $contractsPendingAssignment = [];
        $contractsCompleted = [];
        $contractsInProgress = [];
        $contractsPendingCompletion = [];
        foreach ($contracts as $contract) {
            $workerAssigned = $contract->getAttribute('worker_assigned');
            $employerAssigned = $contract->getAttribute('employer_assigned');
            $workerAgreement = $contract->getAttribute('worker_completion_agreement');
            $employerAgreement = $contract->getAttribute('employer_completion_agreement');

            if ($workerAgreement && $employerAgreement) {
                $contractsCompleted[] = $contract;
            } elseif ($workerAgreement || $employerAgreement) {
                $contractsPendingCompletion[] = $contract;
            } elseif ($workerAssigned && $employerAssigned) {
                $contractsInProgress[] = $contract;
            } else {
                $contractsPendingAssignment[] = $contract;
            }
        }

        $sortedContracts = [
            'pending_assignment' => $contractsPendingAssignment,
            'in_progress' => $contractsInProgress,
            'completed' => $contractsCompleted,
            'pending_completion' => $contractsPendingCompletion,
        ];

        return response()->json($sortedContracts, Response::HTTP_OK);
    }

    public function showWorkerResumes(Worker $worker): JsonResponse
    {
        $resumes = Resume::with(
            [
                'role:id,domestic_role_name',
                'city:id,city_name',
                'schedule.monday',
                'schedule.tuesday',
                'schedule.wednesday',
                'schedule.thursday',
                'schedule.friday',
                'schedule.sunday',
                'schedule.saturday',
            ]
        )
            ->where('worker_id', $worker->id)
            ->orderByDesc('created_at')
            ->get()
            ->all();

        return response()->json($resumes, Response::HTTP_OK);
    }

    public function update(Request $request, Worker $worker): JsonResponse
    {
        $updatedWorker = $worker->fill(array_filter([
            'firstname' => $request->input('firstName'),
            'lastname' => $request->input('lastName'),
            'partonymic' => $request->input('patronymic'),
            'phone_number' => $request->input('phoneNumber'),
            'total_experience' => $request->input('totalExperience'),
            'description' => $request->input('description'),
            'gender' => $request->input('gender'),
            'age' => $request->input('age'),
            'password' => Hash::make($request->input('password'))
        ]));

        if ($request->input('city')) {
            $city = City::where('city_name', $request->input('city'))->first();
            $worker->city()->associate($city);
        }

        if ($request->file('avatar')) {
            $file = $request->file('avatar');
            $fileName = time() . '.' . $file->getClientOriginalExtension();

            $file->storeAs('public/avatars', $fileName);

            $updatedWorker = $worker->fill(['avatar' => $fileName]);
        }

        if ($request->input('additionalInfo')) {
            $data = json_decode($request->input('additionalInfo'), true);

            $additionalInfo = AdditionalInfo::create(array_filter([
                'smoking' => $data['smoking'],
                'foreign_passport' => $data['foreignPassport'],
                'ready_for_moving' => $data['readyForMoving'],
                'ready_for_traveling' => $data['readyForTraveling'],
                'have_children' => $data['haveChildren'],
                'driver_licence' => $data['driverLicence'],
                'nonconviction_certificate' => $data['nonconvictionCertificate'],
                'medicine_certificate' => $data['medicineCertificate'],
                'mental_state_certificate' => $data['mentalStateCertificate'],
                'medical_tests' => $data['medicalTests'],
                'can_swimming' => $data['canSwimming'],
            ]));


            if (array_key_exists('educations', $data)) {
                $educationIdArray = [];
                foreach ($data['educations'] as $educationBranch) {
                    $educationId = Education::where('education_branch', $educationBranch)->first()->id;
                    $educationIdArray[] = $educationId;
                }
                $additionalInfo->educations()->sync($educationIdArray);
            }

            if (array_key_exists('languages', $data)) {
                $languageIdArray = [];
                foreach ($data['languages'] as $language) {
                    $languageId = Language::where('language_name', $language)->first()->id;
                    $languageIdArray[] = $languageId;
                }
                $additionalInfo->languages()->sync($languageIdArray);
            }

            if (array_key_exists('citizenships', $data)) {
                $citizenshipIdArray = [];
                foreach ($data['citizenships'] as $citizenship) {
                    $citizenshipId = Citizenship::where('citizenship_name', $citizenship)->first()->id;
                    $citizenshipIdArray[] = $citizenshipId;
                }
                $additionalInfo->citizenships()->sync($citizenshipIdArray);
            }

            $worker->additionalInfo()->associate($additionalInfo);
        }

        $worker->save();

        $worker->city;
        $worker->additionalInfo;
        if ($worker->additionalInfo) {
            $worker->additionalInfo->educations;
            $worker->additionalInfo->languages;
            $worker->additionalInfo->citizenships;
        }

        return response()->json($updatedWorker, Response::HTTP_OK);
    }
}
