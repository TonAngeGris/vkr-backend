<?php

namespace App\Http\Controllers;

use App\Models\Citizenship;
use Illuminate\Http\Request;

class CitizenshipController extends Controller
{
    public function index(Request $request)
    {
        $citizenshipName = $request->query('query');

        $queryResult = Citizenship::query()
            ->where('citizenship_name', 'ILIKE', "%$citizenshipName%")
            ->orderBy('citizenship_name')
            ->get()
            ->all();

        $citizenships = [];
        foreach ($queryResult as $item) {
            $citizenships[] = $item->getAttribute('citizenship_name');
        }

        return response()->json($citizenships, 200);
    }
}
