<?php

namespace App\Http\Requests;

use App\Traits\ValidationTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RegisterEmployerRequest extends FormRequest
{
    use ValidationTrait;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'firstName' => 'required|string|max:50',
            'lastName' => 'required|string|max:50',
            'patronymic' => 'string|max:50',
            'city' => 'required',
            'phoneNumber' => 'required|string|size:12|unique:employer,phone_number',
            'age' => 'required|numeric|min:16|max:200',
            'gender' => [
                'required',
                Rule::in(['М', 'Ж'])
            ],
            'password' => 'required|string|min:6|max:128',
            'repeatedPassword' => 'required|same:password',
        ];
    }
}