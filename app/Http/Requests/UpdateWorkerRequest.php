<?php

namespace App\Http\Requests;

use App\Models\Worker;
use App\Traits\ValidationTrait;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateWorkerRequest extends FormRequest
{
    use ValidationTrait;

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        $userId = auth()->user()->id;

        return [
            'firstName' => 'string|max:50',
            'lastName' => 'string|max:50',
            'patronymic' => 'string|max:50',
            'phoneNumber' => ['string', 'size:12', Rule::unique(Worker::class, 'phone_number')->ignore($userId)],
            'age' => 'numeric|min:16|max:200',
            'totalExperience' => 'numeric|lt:age',
            'gender' => [
                Rule::in(['М', 'Ж'])
            ],
            'password' => 'string|min:6|max:128|nullable',
            'repeatedPassword' => 'same:password',
            'avatar' => 'image|mimes:png,jpg,jpeg,webp|max:16384'
        ];
    }
}
